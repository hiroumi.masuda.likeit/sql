CREATE DATABASE PRACTICE DEFAULT CHARACTER SET utf8;

USE PRACTICE; 

CREATE TABLE item_category(
  category_id int PRIMARY KEY AUTO_INCREMENT,
  category_name VARCHAR(256) NOT NULL
);

CREATE TABLE item(
  item_id int PRIMARY KEY AUTO_INCREMENT,
  item_name VARCHAR(256) NOT NULL,
  item_price int NOT NULL,
  category_id int
);

   
insert into item_category(category_id,category_name) values (1,'家具');
insert into item_category(category_id,category_name) values (2,'食品');
insert into item_category(category_id,category_name) values (3,'本');

insert into item(item_name,item_price,category_id)
  values
  ('堅牢な机',3000,1),
  ('生焼け肉',50,2),
  ('すっきりわかるJava入門',3000,3),
  ('おしゃれな椅子',2000,1),  
  ('こんがり肉',500,2),
  ('書き方ドリルSQL',2500,3),
  ('ふわふわのベッド',30000,1),  
  ( 'ミラノ風ドリア',300,2) ;
  
 select item_name,item_price from item
 where category_id=1;

select item_name,item_price from item
  where item_price>=1000;

select item_name,item_price from item
 where item_name like'%肉%'
 
select
item_id,
item_name,
item_price,
category_name
from
item
inner join
item_category
on
item.category_id=item_category.category_id;

select
 category_name,
 SUM(item_price) as total_price
 from
 item
 inner join
 item_category
 on
 item.category_id=item_category.category_id
 group by
 category_name
 order by
 total_price desc;
